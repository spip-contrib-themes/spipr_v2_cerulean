<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-theme_bscerulean
// Langue: fr
// Date: 25-03-2020 09:49:32
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'theme_bs4cerulean_description' => 'A calm, blue sky',
	'theme_bs4cerulean_slogan' => 'A calm, blue sky',
);
?>